<?php

class REAXMLParser {

    private $xml_path;
    private $xml;

    public function __construct($xml_path) {
        $this->xml_path = $xml_path;

        $this->load();
    }

    private function load() {

        try {

            if (!file_exists($this->xml_path)) {
                throw new Exception("$this->xml_path cant be found");
            }

            $this->xml = simplexml_load_file($this->xml_path);
        } catch (Exception $ex) {
            throw $ex;
        }
    }

    public function getPropertyTypes() {

        $property_types = [];

        foreach ($this->xml->children() as $child) {
            $property_types[(string) $child->uniqueID] = $child->getName();
        }

        return $property_types;
    }

    public function averagePricePerState() {

        $result = [];
        $average = [];

        foreach ($this->xml->children() as $child) {

            if ((int) $child->price != 0) {
                $result[(string) $child->address->state][] = (int) $child->price;
            }
        }

        foreach ($result as $state => $prices) {
            $average[$state] = array_sum($prices) / count($prices);
        }

        return $average;
    }

}
