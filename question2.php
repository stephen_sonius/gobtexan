<?php

//Write a PHP function that concatenate/merge two strings character by character.
//E.g : MICHAEL JACKSON = MJIACCHKASEOLN


function concatenate(string $str1, string $str2): string {

    $result = [];
    $arr1 = str_split($str1);
    $arr2 = str_split($str2);

    foreach ($arr1 as $key => $val) {
        $result[] = $val;
        $result[] = $arr2[$key] ?? '';
    }

    return implode('', $result);
}

$str1 = 'MICHAEL';
$str2 = 'JACKSON';

$merged_string = concatenate($str1, $str2);

echo $merged_string;